﻿////////////////////////////////////////////////////////////////////////////////
//
//    Read Tags
//
////////////////////////////////////////////////////////////////////////////////

using System;
using Impinj.OctaneSdk;
using System.IO;
using System.Collections.Generic;
using ReadTags;
using System.IO.Pipes;
using System.Diagnostics;
using System.Threading;
using System.Linq;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace OctaneSdkExamples
{

    class DoubleBufferedForm : Form {
        public DoubleBufferedForm() {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.DoubleBuffer, true);
            this.DoubleBuffered = true;
        } 
    }

    class Program {

        static ImpinjReader reader = new ImpinjReader();
        static FileStream fs;
        static StreamWriter sw;
        private static object writeLock = new object();
        // --
        static List<String> tag_names = new List<String>();
        static Dictionary<String, int> tag_name2index = new Dictionary<String, int>();
        // --
        static long t_0 = BernTime.TimeInMilliseconds;
        // --
        static DoubleBufferedForm f = new DoubleBufferedForm();
        static BufferedGraphicsContext dc = new BufferedGraphicsContext();
        static BufferedGraphics backbuffer;
        static Graphics g;
        static List<GraphicsPath> gx_vec = new List<GraphicsPath>();
        static List<Pen> p_vec = new List<Pen>();
        static List<Color> color_vec = new List<Color>();
        static List<Queue<long>> prev_time_queue_vec = new List<Queue<long>>();
        static List<Queue<double>> prev_phase_queue_vec = new List<Queue<double>>();
        // -- // FORNOW
        static List<GraphicsPath> gx_AOA_vec = new List<GraphicsPath>();
        static List<Pen> p_AOA_vec = new List<Pen>();
        static List<Color> color_AOA_vec = new List<Color>();
        static List<List<int>> window_spec_vec = new List<List<int>>();
        static List<double> prev_AOA_vec = new List<double>();
        static List<double> prev_time_AOA_vec = new List<double>();

        static GraphicsPath gx_bounds_1 = new GraphicsPath();
        static GraphicsPath gx_bounds_2 = new GraphicsPath();
        static GraphicsPath gx_bounds_3 = new GraphicsPath();
        static Pen p_bounds = new Pen(Color.Gray, 5);
        // --
        static float x_fac = (float)(1.0 / 20.0);
        static float y_offset = (float)50.0;

        static bool DRAW_TRACES = false;
        static bool CONSOLE_OUTPUT = false;
        static bool DRAW_AOA = true;
        static MLApp.MLApp matlab;

        static int FRAME_i = 0;
        static int AOA_EVERY_X_FRAMES = 50;
        static int NUM_TO_SEND = 5;

        static double max_t = 0.0;

        static void WriteToFile(object msg) {
            lock (writeLock) {
                NamedPipeServerStream server = new NamedPipeServerStream("DemoServer");
                server.WaitForConnection();
                // --
                MemoryStream stream = new MemoryStream();
                using (BinaryWriter writer = new BinaryWriter(stream)) {
                    writer.Write((string)msg);
                    server.Write(stream.ToArray(), 0, stream.ToArray().Length);
                }
                // --
                stream.Close();
                server.Disconnect();
                server.Close();
            }
        }

        static void Main(string[] args) {
            try {
                // (-1) matlab binding
                if (DRAW_AOA) {
                    matlab = new MLApp.MLApp();
                    matlab.Execute(@"cd C:\Users\Jim\Desktop\cpp\simulationandcontrolplaygroundcode\Plush");
                }
 
                // (0) Load tag names.
                var fs_tags = new FileStream("../../input/input.tags", FileMode.Open, FileAccess.Read);
                using (var sr_tags = new StreamReader(fs_tags)) {
                    string line;
                    while ((line = sr_tags.ReadLine()) != null) {
                        tag_names.Add(line);
                    }
                }
                // (#) Load windows
                List<int> window_A = new List<int>();
                List<int> window_B = new List<int>();
                // List<int> window_C = new List<int>();
                // List<int> window_D = new List<int>();
                // List<int> window_E = new List<int>();
                window_A.Add(0); window_A.Add(1); window_A.Add(2 ); window_A.Add(3 );
                window_B.Add(4); window_B.Add(5); window_B.Add(6 ); window_B.Add(7 );
                // window_C.Add(8); window_C.Add(9); window_C.Add(10); window_C.Add(11);
                // window_D.Add(6); window_D.Add(7); window_D.Add(8 ); /*window_D.Add(9 );
                // window_E.Add(8); window_E.Add(9); window_E.Add(10); window_E.Add(11);
                window_spec_vec.Add(window_A);
                window_spec_vec.Add(window_B);
                // window_spec_vec.Add(window_C);
                // window_spec_vec.Add(window_D);
                // window_spec_vec.Add(window_E);

                // -- phase
                color_vec.Add(Color.Red);
                color_vec.Add(Color.Orange);
                color_vec.Add(Color.Yellow);
                color_vec.Add(Color.Green);
                color_vec.Add(Color.Cyan);
                color_vec.Add(Color.Blue);
                color_vec.Add(Color.Indigo);
                color_vec.Add(Color.Violet);
                for (var i = 0; i < tag_names.Count(); ++i) {
                    Queue<double> prev_phase_queue = new Queue<double>();
                    Queue<long> prev_time_queue = new Queue<long>();
                    for (var _ = 0; _ < NUM_TO_SEND; ++_) {
                        prev_phase_queue.Enqueue(0);
                        prev_time_queue.Enqueue(0);
                    }
                    prev_phase_queue_vec.Add(prev_phase_queue);
                    prev_time_queue_vec.Add(prev_time_queue);
                    // --
                    gx_vec.Add(new GraphicsPath());
                    p_vec.Add(new Pen(color_vec[i % color_vec.Count]));
                }

                // -- aoa
                color_AOA_vec.Add(Color.Red);
                // color_AOA_vec.Add(Color.Yellow);
                color_AOA_vec.Add(Color.LimeGreen);
                // color_AOA_vec.Add(Color.Cyan);
                color_AOA_vec.Add(Color.Cyan);
                for (var i = 0; i < window_spec_vec.Count(); ++i) {
                    prev_AOA_vec.Add(0);
                    prev_time_AOA_vec.Add(0);
                    gx_AOA_vec.Add(new GraphicsPath());
                    p_AOA_vec.Add(new Pen(color_AOA_vec[i % color_AOA_vec.Count], 5));
                }

                // -- spawn graphics
                f.Size = new Size(1000, 900);
                f.Show();
                g = f.CreateGraphics();
                backbuffer = dc.Allocate(g, new Rectangle(new Point(0, 0), g.VisibleClipBounds.Size.ToSize()));

                // -- build reverse lookup
                for (var i = 0; i < tag_names.Count; ++i) {
                    var name = tag_names[i];
                    tag_name2index.Add(name, i);
                }

                // -- print for convenience
                for (var i = 0; i < tag_names.Count; ++i) {
                    Console.WriteLine("{0}: {1}", i, tag_names[i]);
                }
                Console.WriteLine("------------------------");

                // --
                fs = new FileStream("../../output/test.data", FileMode.Create);
                sw = new StreamWriter(fs);
                // --
                Console.WriteLine("Attempting to connect to reader..."); {
                    reader.Connect(SolutionConstants.ReaderHostname);
                    System.Media.SystemSounds.Beep.Play();
                } Console.WriteLine("Connected to reader.");
                // --
                Settings settings = reader.QueryDefaultSettings();
                settings.Report.IncludePhaseAngle = true;
                settings.Report.IncludeChannel = false;
                settings.Report.IncludePeakRssi = false;
                settings.Report.IncludeAntennaPortNumber = false;
                settings.Report.Mode = ReportMode.Individual;
                settings.ReaderMode = ReaderMode.MaxThroughput;
                settings.SearchMode = SearchMode.DualTarget;
                settings.TagPopulationEstimate = 12; // (*)
                settings.Antennas.GetAntenna(1).IsEnabled = true;
                settings.Antennas.GetAntenna(1).MaxTxPower = true;
                settings.Antennas.GetAntenna(1).MaxRxSensitivity = true;
                // TODO: Are there more of these? // (*)
                // --
                reader.ApplySettings(settings);
                reader.TagsReported += OnTagsReported;
                reader.TagsReported += Draw;
                reader.Start();
                // --
                Console.WriteLine("Press any key to end session...");
                Console.ReadKey();
                // --
                reader.Stop();
                sw.Close();
                reader.Disconnect();
                System.Environment.Exit(1);
            }
            // --
            catch (OctaneSdkException e) { Console.WriteLine("Octane SDK exception: {0}", e.Message); }
            catch (Exception e) { Console.WriteLine("Exception : {0}", e.Message); }
        }

        static void OnTagsReported(ImpinjReader sender, TagReport report) {

            ++FRAME_i;

            foreach (Tag tag in report) {
                string name = tag.Epc.ToString();
                // tag.LastSeenTime
                // https://stackoverflow.com/questions/31129056/check-if-string-exist-in-list
                if (tag_names.Where(o => string.Equals(name, o, StringComparison.OrdinalIgnoreCase)).Any()) {
                    int i = tag_name2index[name];
                    double phase = tag.PhaseAngleInRadians * 180.0 / Math.PI;
                    long time = BernTime.TimeInMilliseconds - t_0;
                    max_t = Math.Max(max_t, time);
                    // --
                    // string data_line = String.Format("tag {0} | {1:F1} deg | {2} ms", i, phase, time);
                    string data_line = String.Format("{0} {1:F1} {2}", i, phase, time);
                    if (CONSOLE_OUTPUT) {
                        Console.WriteLine(data_line);
                    }
                    sw.WriteLine(data_line);
                    // -- 
                    if (DRAW_TRACES) {
                        gx_vec[i].AddLine(
                                   (float)prev_time_queue_vec[i].Last() * x_fac,
                                   (float)prev_phase_queue_vec[i].Last() + y_offset,
                                   (float)time * x_fac,
                                   (float)phase + y_offset
                                   );
                    }
                    // --
                    prev_time_queue_vec[i].Dequeue();
                    prev_phase_queue_vec[i].Dequeue();
                    prev_time_queue_vec[i].Enqueue(time);
                    prev_phase_queue_vec[i].Enqueue(phase);
                }
            }

            if (DRAW_AOA) {
                if (FRAME_i % AOA_EVERY_X_FRAMES == 0) {
                    for (var i = 0; i < window_spec_vec.Count; ++i) {
                        List<int> window_spec = window_spec_vec[i];
                        int NUM_TAGS = window_spec.Count; // tag_names.Count();
                        // -- // setup
                        double[,] phaseMatrix = new double[NUM_TO_SEND, NUM_TAGS];
                        for (var j = 0; j < NUM_TAGS; ++j) {
                            for (var k = 0; k < NUM_TO_SEND; ++k) {
                                phaseMatrix[k, j] = prev_phase_queue_vec[window_spec[j]].ElementAt(k) * (Math.PI / 180.0);
                            }
                        }
                        double numSensors = NUM_TAGS;
                        double spacing = 6 * (.01);
                        double freq = 8.65e8;
                        // -- // calculate 
                        object result = null;
                        matlab.Feval("getOneBroadside", 2, out result, phaseMatrix, numSensors, spacing, freq);
                        object[] res = result as object[];
                        // -- // post
                        double success_proxy = (double)res[0];
                        double AOA = prev_AOA_vec[i];
                        if (success_proxy > .5) {
                            AOA = (double) res[1];
                        }

                        if (true) {
                            long time_AOA = BernTime.TimeInMilliseconds - t_0;
                            // AOA = 90*Math.Sin(.5*i + .001*time_AOA); // FORNOW
                            gx_AOA_vec[i].AddLine(
                                       (float)prev_time_AOA_vec[i] * x_fac,
                                       (2f * (float)prev_AOA_vec[i] + y_offset + 180f),
                                       (float)time_AOA * x_fac,
                                       (2f * (float)AOA + y_offset + 180f)
                                       );
                            prev_time_AOA_vec[i] = time_AOA;
                            prev_AOA_vec[i] = AOA;
                            // -- NOTE: just bounds here
                            float x1 = (float)prev_time_AOA_vec.Max() * x_fac;
                            float x2 = (float)time_AOA * x_fac;
                            gx_bounds_1.AddLine(x1, 0f + y_offset, x2, 0f + y_offset);
                            gx_bounds_2.AddLine(x1, +180f + y_offset, x2, +180f + y_offset);
                            gx_bounds_3.AddLine(x1, +360f + y_offset, x2, +360f + y_offset);
                        }
                    }
                }
            } 
        }

        static void Draw(ImpinjReader sender, TagReport report) {

            float tx = (float) max_t * x_fac;
            backbuffer.Graphics.ResetTransform();
            backbuffer.Graphics.TranslateTransform(1000 - tx, 0); 
            backbuffer.Graphics.Clear(Color.Black); // FORNOW 
            // --
            if (DRAW_TRACES) {
                for (var i = 0; i < tag_names.Count; ++i) {
                    backbuffer.Graphics.DrawPath(p_vec[i], gx_vec[i]);
                }
            }
            if (DRAW_AOA) {
                for (var i = 0; i < window_spec_vec.Count; ++i) {
                    backbuffer.Graphics.DrawPath(p_AOA_vec[i], gx_AOA_vec[i]);
                }
            }
            backbuffer.Graphics.DrawPath(p_bounds, gx_bounds_1);
            backbuffer.Graphics.DrawPath(p_bounds, gx_bounds_2);
            backbuffer.Graphics.DrawPath(p_bounds, gx_bounds_3);
            // --
            backbuffer.Render(g); // FORNOW

            if (FRAME_i % 10000 == 0) {
                foreach (GraphicsPath gx in gx_vec) {
                    gx.Reset();
                }
                foreach (GraphicsPath gx_AOA in gx_AOA_vec) {
                    gx_AOA.Reset();
                }
                gx_bounds_1.Reset();
                gx_bounds_2.Reset();
                gx_bounds_3.Reset();
            } 
        } 
    }
}
